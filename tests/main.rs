use rust_jq::*;

/// Run this test with
///
///     cargo test -- --nocapture
///
/// for all useful information.
#[test]
fn all() {
    let tree = parse(JSON).collect::<Vec<_>>();
    //eprintln!("tree: {:?}", tree);
    assert_eq!(tree.len(), 30);

    for (mut p, v) in tree.into_iter() {
        eprintln!("{} {:?}", p.path(), v);
    }
}
