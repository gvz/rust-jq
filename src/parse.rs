use super::token;
use token::Token;

#[derive(Debug, Clone)]
pub enum Path<'a> {
    Obj,
    Array(u32),
    Key(&'a str),
}

#[derive(Debug, PartialEq)]
pub enum Value<'a> {
    JNull,
    JBool(bool),
    JStr(&'a str),
    JNum(u32),
    Done,
}

impl<'a> Value<'a> {
    pub fn as_str(self) -> &'a str {
        if let Value::JStr(s) = self {
            s
        } else {
            panic!(format!("{:?} is not a string value", self))
        }
    }
}

type Stack<'a> = Vec<Path<'a>>;

fn path_str(stack: &Stack<'_>, result: &mut String) -> std::fmt::Result {
    use std::fmt::Write;
    result.clear();
    for path in stack {
        match path {
            Path::Obj => write!(result, ".")?,
            Path::Array(n) => write!(result, "[{}]", n)?,
            Path::Key(name) => write!(result, "{}", name)?,
        }
    }
    Ok(())
}

#[derive(Debug, Clone)]
pub struct Parse<'a> {
    token: Token<'a>,
    pub stack: Stack<'a>,
    c_path: String,
}
impl<'a> Parse<'a> {
    pub fn new(token: Token<'a>) -> Self {
        Self {
            token,
            stack: Default::default(),
            c_path: Default::default(),
        }
    }

    pub fn path(&mut self) -> &str {
        path_str(&self.stack, &mut self.c_path).expect("write to string");
        &self.c_path
    }

    pub fn next(self) -> (Self, Value<'a>) {
        let Self {
            token,
            mut stack,
            mut c_path,
        } = self;

        let (token, value) = token.next();
        //eprintln!("next @ {:?} + {:?}", stack, value);

        if let token::Value::Space = value {
            return Self {
                token,
                stack,
                c_path,
            }
            .next();
        } else if let token::Value::ObjStart = value {
            stack.push(Path::Obj);
            return Self {
                token,
                stack,
                c_path,
            }
            .next();
        } else if let token::Value::Str(val) = value {
            if let Some(Path::Obj) = stack.last() {
                stack.push(Path::Key(val));
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            } else {
                let next = Self {
                    token,
                    stack,
                    c_path,
                };
                let val = Value::JStr(val);
                return (next, val);
            }
        } else if let token::Value::ObjKeyAssoc = value {
            if let Some(Path::Key(_)) = stack.last() {
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            }
        } else if let token::Value::ArrayStart = value {
            stack.push(Path::Array(0));
            return Self {
                token,
                stack,
                c_path,
            }
            .next();
        } else if let token::Value::ObjEnd = value {
            if let Some(Path::Obj) = stack.last() {
                stack.pop();
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            } else if let Some(Path::Key(_)) = stack.last() {
                stack.pop();
                stack.pop();
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            }
        } else if let token::Value::ListSep = value {
            if let Some(Path::Array(n)) = stack.last_mut() {
                *n += 1;
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            } else if let Some(Path::Key(_)) = stack.last() {
                stack.pop();
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            }
        } else if let token::Value::ArrayEnd = value {
            if let Some(Path::Array(_)) = stack.last() {
                stack.pop();
                return Self {
                    token,
                    stack,
                    c_path,
                }
                .next();
            }
        } else if let token::Value::Bool(val) = value {
            let next = Self {
                token,
                stack,
                c_path,
            };
            let val = Value::JBool(val);
            return (next, val);
        } else if let token::Value::Num(val) = value {
            let next = Self {
                token,
                stack,
                c_path,
            };
            let val = Value::JNum(val);
            return (next, val);
        } else if let token::Value::Null = value {
            let next = Self {
                token,
                stack,
                c_path,
            };
            let val = Value::JNull;
            return (next, val);
        } else if let token::Value::Done = value {
            if let None = stack.last() {
                let next = Self {
                    token,
                    stack,
                    c_path,
                };
                let val = Value::Done;
                return (next, val);
            }
        }

        let path_result = path_str(&stack, &mut c_path);
        panic!(format!(
            "Cannot handle {:?} in {} ({:?})",
            value, c_path, path_result
        ))
    }
}
