#[derive(Debug, PartialEq)]
pub enum Value<'a> {
    Done,
    Space,
    Null,
    Str(&'a str),
    Id(&'a str),
    Num(u32),
    Bool(bool),
    ListSep,
    ArrayStart,
    ArrayEnd,
    ObjStart,
    ObjKeyAssoc,
    ObjEnd,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Token<'a>(pub &'a str);

impl<'a> Token<'a> {
    pub fn skip(self, n: usize) -> Self {
        Self(&self.0[n..])
    }

    pub fn next(self) -> (Self, Value<'a>) {
        if self.0.is_empty() {
            (self, Value::Done)
        } else {
            match &self.0[0..1] {
                " " | "\n" => (Self(self.0.trim_start()), Value::Space),
                "{" => (self.skip(1), Value::ObjStart),
                "\"" => {
                    let mut end: usize = 0;
                    let mut prev: Option<u8> = None;
                    let rest = &self.0[1..];
                    let bytes = rest.bytes();

                    for b in bytes {
                        end += 1;

                        if b == b'"' && prev != Some(b'\\') {
                            break;
                        } else {
                            prev = Some(b);
                        }
                    }

                    if end == 0 {
                        panic!("Unterminated string")
                    }

                    let val = &rest[0..end - 1];
                    let next = self.skip(val.len() + 2);
                    (next, Value::Str(val))
                }
                ":" => (self.skip(1), Value::ObjKeyAssoc),
                "," => (self.skip(1), Value::ListSep),
                "[" => (self.skip(1), Value::ArrayStart),
                "]" => (self.skip(1), Value::ArrayEnd),
                "}" => (self.skip(1), Value::ObjEnd),
                _ => {
                    let mut chars = self.0.chars();
                    match chars.next() {
                        None => (self, Value::Done),
                        Some(c) if c.is_alphabetic() => {
                            let len = 1 + chars.take_while(|c| c.is_alphabetic()).count();
                            let val = &self.0[0..len];
                            let next = self.skip(len);
                            let val = match val {
                                "true" => Value::Bool(true),
                                "false" => Value::Bool(false),
                                "null" => Value::Null,
                                val => Value::Id(val),
                            };
                            (next, val)
                        }
                        Some(c) if c.is_digit(10) => {
                            let len = 1 + chars.take_while(|c| c.is_digit(10)).count();
                            let val = &self.0[0..len];
                            let val: u32 = val.parse().expect("u32 number");
                            let next = self.skip(len);
                            (next, Value::Num(val))
                        }
                        Some(c) => panic!(format!("Cannot handle this: {}", c)),
                    }
                }
            }
        }
    }
}
