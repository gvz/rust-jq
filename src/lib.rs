pub mod parse;
mod token;

pub const JSON: &str = r#"
    {
        "abject": [{},{},[]],
        "bool": true,
        "false": false,
        "string": "hello pop",
        "number": 23,
        "array": [
            true, true, false, "hello", "pop", 24],
        "object": {
            "bal": true,
            "fals": false,
            "strang": "halla pap",
            "nambra": 32,
            "ororo": [
                false, false, true, "hollel", "pep", 42]
        },
        "fup": [
        { "a": [1,2,3],
            "b": {
                "c": [4,[5,[6,{"7": 8, "9": {}, "10": [11, 12]}]]]}}],

        "now with \"STRING ESCAPES!\" as \"\" well ; ) \"\"": null,
    }
    "#;

pub fn main() {
    scar();
}

pub struct ParseIter<'a> {
    parser: parse::Parse<'a>,
}

impl<'a> Iterator for ParseIter<'a> {
    type Item = (parse::Parse<'a>, parse::Value<'a>);
    fn next(&mut self) -> Option<Self::Item> {
        let (parser, value) = self.parser.clone().next();
        self.parser = parser.clone();
        if value == parse::Value::Done {
            None
        } else {
            Some((parser, value))
        }
    }
}

pub fn parse(src: &str) -> ParseIter<'_> {
    let parser = parse::Parse::new(token::Token(src));
    let iter = ParseIter { parser };
    iter
}

pub fn scar() {
    let mut parser = parse::Parse::new(token::Token(JSON));
    loop {
        let (p2, val) = parser.next();
        parser = p2;

        if val == parse::Value::Done {
            break;
        } else {
            println!("{}: {:?}", parser.path(), val);
        }
    }
}
