pub struct Buffer<'a, T> {
    bytes: &'a [T],
    first: usize,
    last: usize,
}

impl <'a, T> Buffer<'a, T> {
    pub fn wrap(bytes:&'a [T]) -> Self {
        Self {
            bytes,
            first: 0,
            last: bytes.len(),
        }
    }
}

